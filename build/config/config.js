$(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
    //Tooltip
    $('[data-toggle="tooltip"]').tooltip();
})
// Daterange Picker format
$(function() {
    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('.selectdaterange span').html(start + ' - ' + end);
    }

    $('.selectdaterange').daterangepicker({
        alwaysShowCalendars: true,
        startDate: start,
        endDate: end,
        maxDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);
});
// Singel Date Picker format
$(function() {
    $('.selectSingleDate').daterangepicker({
        "singleDatePicker": true,
        "startDate": "01/01/1990",
        "endDate": moment()
    }, 
    function(start, end, label) {
        console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
    });
});